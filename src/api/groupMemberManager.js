import fetch from 'utils/fetch';
import Qs from 'qs';
var groupMemberManager = {
    getList: function(pageSize, pageNo) {
        return fetch({
            url: '/member/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },
    editUser: function(data) {
        return fetch({
            url: '/member/' + data.pid,
            method: 'post',
            data: data
        })
    }
}
export default groupMemberManager;