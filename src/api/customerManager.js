import fetch from 'utils/fetch';
import Qs from 'qs';
var customerManager = {

    //获取客户列表
    getList: function(pageSize, pageNo) {
        return fetch({
            url: '/member/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'post'
        })
    },

    //搜索
    searchList: function(pageSize, pageNo, data) {
        return fetch({
            url: '/member/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'post',
            data: data
        })
    },

    //编辑用户保存信息
    saveEditUser: function(data) {
        return fetch({
            url: '/member/' + data.id,
            method: 'put',
            data: data
        })
    },

    //获取标签列表
    getLabelList: function(pageSize, pageNo) {
        return fetch({
            url: '/label/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },

    //搜索标签
    searchLabelList: function(pageSize, pageNo, data) {
        return fetch({
            url: '/label/list?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + data,
            method: 'get'
        })
    },

    //同步公众号粉丝
    syncFans: function() {
        return fetch({
            url: '/member/sync',
            method: 'get'
        })
    },

    // 同步标签/分组
    syncLabel: function() {
        return fetch({
            url: '/label/sync',
            method: 'get'
        })
    },

    //添加标签
    addLabel: function(data) {
        return fetch({
            url: '/label/add',
            method: 'post',
            data: data
        })
    },

    //更新标签
    refreshLabel: function(data) {
        return fetch({
            url: '/label/' + data.id,
            method: 'put',
            data: data
        })
    },

    //删除标签
    deleteLabel: function(data) {

        return fetch({
            url: '/label/' + data.id,
            method: 'delete',
            data: data
        })
    },

    //判断会员编号是否相同
    memberNoSame: function(data) {
        var url = '/member/checkMemberNo?memberNo=' + data.memberNo + '&id=' + data.id
        if (typeof data.id == "undefined") {
            url = '/member/checkMemberNo?memberNo=' + data.memberNo;
        }
        return fetch({
            url: url,
            method: 'get'
        })
    },

    //根据昵称获取openID
    getMember: function(pageSize, nickName) {
        return fetch({
            url: '/fans/remoteSearch?pageSize=' + pageSize + '&nickName=' + nickName,
            method: 'get'
        })
    },

    //增加会员
    addMember: function(data) {
        return fetch({
            url: '/member/add',
            method: 'post',
            data: data
        })
    },

    //删除会员
    deleteMember: function(data) {
        return fetch({
            url: '/member/' + data.id,
            method: 'delete',
            data: data
        })
    },

    //更新绑定状态
    updateBanding: function(data) {
        return fetch({
            url: '/member/' + data.id,
            method: 'put',
            data: data
        })
    },
    //绑定
    bind: function(data) {
        return fetch({
            url: '/member/bind/' + data.id,
            method: 'put',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: Qs.stringify({ openID: data.openID })
        })
    },
    //解除绑定
    unBind: function(data) {
        return fetch({
            url: '/member/bind/' + data.id,
            method: 'delete',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: Qs.stringify({ openID: data.openid })
        })
    },
    //查询未绑定的粉丝

    getUnbindlist: function(pageSize, pageNo, searchText) {
        return fetch({
            url: '/fans/unbindlist?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + searchText,
            method: 'get'
        })
    }

}
export default customerManager;