import fetch from 'utils/fetch';
import Qs from 'qs';
var apiLog = {
    getList: function(pageSize, pageNo, keyword) {
        return fetch({
            url: '/apilogs?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + keyword,
            method: 'get'
        })
    }
}
export default apiLog;