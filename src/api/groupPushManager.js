import fetch from 'utils/fetch';
import Qs from 'qs';
var groupPushManager = {

    getList: function(pageSize, pageNo, keyword, data) {
        return fetch({
            url: '/grouppush/list?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + keyword,
            method: 'post',
            data: data
        })
    },
    add: function(data) {
        return fetch({
            url: '/grouppush/add',
            method: 'post',
            data: data
        })
    },
    edit: function(data) {
        return fetch({
            url: '/grouppush/' + data.id,
            method: 'put',
            data: data
        })
    },
    deleteUser: function(id) {
        return fetch({
            url: '/grouppush/delete?id=' + id,
            method: 'delete'
        })
    },
    push: function(id, data) {
        return fetch({
            url: '/grouppush/push?id=' + id,
            method: 'get',
            data: data
        })
    },
    getMember: function(pageSize, id) {
        return fetch({
            url: '/member/listByID?pageSize=' + pageSize + '&memberNo=' + id,
            method: 'get'
        })
    },
    getGroupList: function(pageSize, pageNo) {
        return fetch({
            url: '/label/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get',
        })
    },
    getTmepalteList: function(pageSize, pageNo) {
        return fetch({
            url: '/template/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },

    getUploadFileList: function(id) {
        return fetch({
            url: '/upload/getUploadList?id=' + id + '&type=2',
            method: 'get'
        })
    },
    uploadCopyFile: function(id, fileStr) {
        var data = {
            id: id,
            type: 2,
            upfile: fileStr
        };
        return fetch({
            url: '/upload/uploadBase64Img',
            method: 'post',
            data: data
        });
    },
    deleteOneFile: function(id, type) {
        return fetch({
            url: '/upload/deleteFile?id=' + id + '&type=' + type,
            method: 'delete'
        })
    },
    getTemplateMessage: function(id, type) {
        return fetch({
            url: '/message/content?msgID=' + id + '&msgType=2',
            method: 'get'
        })
    },
    getTemplateBindParam: function(id) {
        return fetch({
            url: '/template/getParam?tplID=' + id,
            method: 'get'
        })
    },
    newCopyPush: function(id) {
        return fetch({
            url: '/grouppush/' + id,
            method: 'post'
        })
    }
}

export default groupPushManager;