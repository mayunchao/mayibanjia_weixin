import fetch from 'utils/fetch';
import Qs from 'qs';
export function loginByEmail(userName, password) {
    const data = {
        userName,
        password
    };
    //以form-data方式发起请求
    return fetch({
        url: '/account/login',
        method: 'post',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: Qs.stringify(data)
    });
}

export function logout() {
    return fetch({
        url: '/account/logout',
        method: 'post'
    });
}

export function getInfo(token) {
    return fetch({
        url: '/account/findByToken',
        method: 'get',
        params: { token }
    });
}
export function changePwd(newPassword, oldPassword) {
    var data = {
        newPassword,
        oldPassword
    };
    return fetch({
        url: '/account/changePwd',
        method: 'post',
        data: Qs.stringify(data)
    });
}